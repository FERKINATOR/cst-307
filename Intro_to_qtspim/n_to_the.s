# Program Description:  Find the sum of the integers from 1 to N where
#  N is a value input from the keyboard.
#########################################################
# Algorithmic Description in Pseudocode:
# main:    v0 <<  value read from the keyboard (syscall 4)
#		   if (v0 < = 0 ) stop
# 		   t0 = 0;		# t0 is used to accumulate the sum
# 		   While (v0 >= 0)  { t0 = t0 + v0;  v0 = v0 - 1}
# 		   Output to monitor syscall(1) << t0;     goto main
##########################################################
# Register Usage: $t0 is used to accumulate the sum
#			$v0 the loop counter, counts down to zero
##########################################################

			.data
prompt:		.asciiz  	"\n\n   Please Input a value for N = "
result:		.asciiz  	 " The sum of the integers from 1 to N is "
bye:		.asciiz 	 "\n  **** Adios Amigo - Have a good day **** "
			.globl	main
			.text
main:		li		$v0, 4			# system call code for print_str
			la		$a0, prompt		# load address of prompt into a0
			syscall					# print the prompt message
			li		$v0, 5			# system call code for read_int
			syscall					# reads a value of N into v0
			blez	$v0,  done		# if ( v0  < = 0 ) go to done
			li		$t0, 0			# clear $t0 to zero
loop:		add 	$t0, $t0, $v0	# sum of integers in register $t0
			addi	$v0, $v0, -1	# summing  in reverse order
			bnez	$v0, loop		# branch to loop if $v0 is != zero
			li		$v0, 4			# system call code for print_str
			la		$a0, result		# load address of message into $a0
			syscall					# print the string
			li		$v0, 1			# system call code for print_int
			move	$a0, $t0		# a0 = $t0
			syscall					# prints the value  in register $a0
			b 	main
done:		li		$v0, 4			# system call code for print_str
			la		$a0, bye		# load address of msg. into $a0
			syscall					# print the string
			li		$v0, 10			# terminate program
			syscall    				# return control to  system